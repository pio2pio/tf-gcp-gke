.SHELL := /usr/bin/bash

INFRA_DIR := infrastructure/gke
APP_DIR := apps/postgresql
export TF_IN_AUTOMATION ?= true
export TF_LOG ?= TRACE
export TF_LOG_PATH ?= /tmp/$(shell basename $$PWD)-$@-$(shell date +"%Y%m%d-%H%M")

.PHONY: clear
clear:
	clear

initialise:
	terraform-docs --version || \
	TERRAFORM_DOCS_VERSION=v0.15.0; \
	curl -sL https://github.com/terraform-docs/terraform-docs/releases/download/$(TERRAFORM_DOCS_VERSION)/terraform-docs-$(TERRAFORM_DOCS_VERSION)-linux-amd64 -o /usr/local/bin/terraform-docs && chmod +x /usr/local/bin/terraform-docs; \
	terraform-docs --version

	pre-commit --version || pip install pre-commit
	pre-commit install
	pre-commit run --all-files

terraform-init:
	terraform -chdir=$(INFRA_DIR)/ init

terraform-refresh:
	terraform -chdir=$(INFRA_DIR)/ refresh

# DEBUG level logs will be written to /tmp/DIRNAME-plan-TIMESTAMP
terraform-plan: terraform-init
	terraform -chdir=$(INFRA_DIR)/ plan

# DEBUG level logs will be written to /tmp/DIRNAME-apply-TIMESTAMP
terraform-apply: terraform-init
	terraform -chdir=$(INFRA_DIR)/ apply -auto-approve && \
	(git tag --delete "$(INFRA_DIR)" || true) && \
	git tag -a "$(INFRA_DIR)" -m "Tagged automatically during make-apply" && \
	(git push origin --delete "$(INFRA_DIR)" || true) && \
	git push origin "$(INFRA_DIR)"

# Usage: Generate kubeconfig
kubeconfig:
	terraform -chdir=$(INFRA_DIR)/ apply -auto-approve -target local_file.kubeconfig

kubeconfig-verify: kubeconfig
	kubectl cluster-info --kubeconfig $(INFRA_DIR)/kubeconfig

# DEBUG level logs will be written to /tmp/DIRNAME-destroy-TIMESTAMP
terraform-destroy: update-tags
	(git checkout "$(INFRA_DIR)" || true) && \
	cd $(INFRA_DIR)/ && \
	terraform -chdir=$(INFRA_DIR)/ init && \
	terraform -chdir=$(INFRA_DIR)/ destroy --force && \
	rm -rf outputs

# Usage: Tag repository
tag:
	(git tag --delete "$(INFRA_DIR)" || true) && \
 	git tag -a "$(INFRA_DIR)" -m "Tagged manually using make-tag" && \
	(git push origin --delete "$(INFRA_DIR)" || true) && \
	git push origin "$(INFRA_DIR)"

update-tags:
	@git pull --force --tags

HELM_REPO := bitnami
HELM_CHART := postgresql
HELM_POSTGRES_RELEASE := $(HELM_CHART)
HELM_POSTGRES_NAMESPACE := $(HELM_CHART)
HELM_POSTGRES_CHART_VERSION := 10.9.5
export KUBECONFIG = $(PWD)/$(INFRA_DIR)/kubeconfig

helm-repo-add:
	helm repo list | grep $(HELM_REPO) || helm repo add $(HELM_REPO) https://charts.bitnami.com/bitnami

helm-install-plan-postgresql: helm-repo-add
	@helm diff version || helm plugin install https://github.com/databus23/helm-diff
	@cd $(APP_DIR)/
	@helm search repo $(HELM_REPO)/$(HELM_POSTGRES_RELEASE) --version $(HELM_POSTGRES_CHART_VERSION)
	@sops --decrypt $(APP_DIR)/values.secret.enc.yaml | \
	helm diff upgrade --install --context 5 postgresql $(HELM_REPO)/$(HELM_CHART) --version $(HELM_POSTGRES_CHART_VERSION) \
	  --show-secrets \
	  -f $(APP_DIR)/values.yaml \
	  -f - \
	  --namespace  $(HELM_POSTGRES_NAMESPACE)

helm-install-postgresql:
	@cd $(APP_DIR)/
	@sops --decrypt $(APP_DIR)/values.secret.enc.yaml | \
	helm upgrade --install postgresql $(HELM_REPO)/$(HELM_CHART) --version $(HELM_POSTGRES_CHART_VERSION) \
	  -f $(APP_DIR)/values.yaml \
	  -f - \
	  --namespace  $(HELM_POSTGRES_NAMESPACE) \
	  --create-namespace

helm-uninstall-postgresql:
	helm uninstall $(HELM_POSTGRES_RELEASE) --namespace $(HELM_POSTGRES_NAMESPACE)

sops-install:
	@which sops || { curl -sSfL https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux > ~/.local/bin/sops && \
	chmod +x ~/.local/bin/sops; }

sops-create-gcp-kms:
	gcloud kms keyrings create-namespace sops --location global
	gcloud kms keys create-namespace sops-key --location global --keyring sops --purpose encryption
	gcloud kms keys list --location global --keyring sops

sops-encrypt: sops-install
	[ -f $(APP_DIR)/values.secret.yaml ] && \
	sops --encrypt $(APP_DIR)/values.secret.yaml > $(APP_DIR)/values.secret.enc.yaml

sops-decrypt:
	sops --decrypt $(APP_DIR)/values.secret.enc.yaml
