# Scope

## GKE cluster creation
The main objective of this assignment is the deployment of a Postgres db in a Google Cloud
Kubernetes cluster.

Terraform cluster creation
You’ll be given a project-id, a service account and a bucket to store the terraform metadata.
You have to automate the creation of a simple GKE cluster with 3 nodes:
* The gke nodes can be public, no need for too much networking configuration.
* Set up a pool of auto-scalable nodes.
* You must provide at the end the kubeconfig
The script/scripts should be saved in a github repository and we should be able to clone the
master branch and execute it for both spinning up the cluster and tearing it down.

## Deployment of a postgres db
You can use any templating tool of your choice (Jsonnet, Helm, Kustomize..) to deploy a
Postgres Database initialised with a pre-created DB “predictx” and user “pi-user” with a
password of your choice.
Expose the k8s service with NodePort so it gets a public IP and port. We should be able to
connect to the db and create a table for example.

## Graduation
In order from more important to less, it will be evaluated:
* Adding documentation with some Mark-down or ascii-doc files along with the code
explaining how to run the scripts, pre-requirements, or whatever you think is
important.
* You might have trouble finishing something, so it’s interesting for us that you
document what problems you have faced or why some part of the assessment you
could not finish.
* As we want you to deliver the work via Git repository (github for example), it’s
desirable that you add proper information about your commits, branches, etc.
The main idea is to collect information about how clean and efficient your work is, how you
think your work can be used by a different person of the team, and how resolutive you are
searching for information that you might not have about a technology, a problem, etc.

# Local development
This is optional step for local development to install [`pre-commit` checks](_doc/pre-commit.md).
```sh
make initialise
```

# Create the stack
## Connect to GCP
```sh
export GOOGLE_CLOUD_KEYFILE_JSON="~/.config/gcloud/application_default_credentials.json"
gcloud config configurations list
gcloud projects list
gcloud auth list
```

## Create gcloud KMS key
```sh
make sops-create-gcp-kms
```

## Create or upgrade GKE Kubernetes cluster
The Terraform module details can be found [here](postgresql/README.md)
```sh
# Initialize Terraform
make terraform-init
make terraform-plan
make terraform-apply

# Generate kubeconfig (expires in 1h)
terraform apply -target local_file.kubeconfig
export KUBECONFIG=$PWD/kubeconfig
```

## Install postgresql
```sh
make helm-install-plan-postgresql # show manifest diff
make helm-install-postgresql
```

## Connect to the database
```sh
# Get connection details from the chart notes.
helm -n postgresql get notes postgresql

# To get the password for "postgres" run:
export POSTGRES_ADMIN_PASSWORD=$(kubectl get secret --namespace postgresql postgresql -o jsonpath="{.data.postgresql-postgres-password}" | base64 --decode)

# To get the password for "pi-user" run:
export POSTGRES_PASSWORD=$(kubectl get secret --namespace postgresql postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

# To connect to your database run the following command:
kubectl run postgresql-client --rm --tty -i --restart='Never' --namespace postgresql --image docker.io/bitnami/postgresql:11.13.0-debian-10-r25 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgresql -U pi-user -d predictx -p 5432

# To connect to your database from outside the cluster execute the following commands:
export NODE_IP=$(kubectl get nodes --namespace postgresql -o jsonpath="{.items[0].status.addresses[0].address}")
export NODE_PORT=$(kubectl get --namespace postgresql -o jsonpath="{.spec.ports[0].nodePort}" services postgresql)
PGPASSWORD="$POSTGRES_PASSWORD" psql --host $NODE_IP --port $NODE_PORT -U pi-user -d predictx
```

## Update passwords
Secrets have been encrypted using [Mozilla SOPS](https://github.com/mozilla/sops) using gcloud KMS service. Follow steps below to reveal the passwords, or update them.
```sh
# decrypt secrets and print to the screen
make sops-decrypt

# Update passwords
## Create decrypted file, so can be edited
make sops-decrypt > apps/postgresql/values.secret.yaml

## Encrypt the apps/postgresql/values.secret.yaml file
make sops-encrypt
```
Once the passwords have been updated you can run `make helm-install-plan-postgresql` to verify.

# Terraform module details
Please see generated terraform docs [here](infrastructure/gke/README.md).

# Tagging
As this is monorepo, on each `make terraform-apply` repository gets tagged. This allows to track the latest code used to configure the cluster, [example](_doc/tagging.md).

# Issues
Please see the [issues](_doc/issues.md).
