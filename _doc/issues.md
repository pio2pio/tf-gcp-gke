# (fixed)Unable to connect database using NodePort.
The database servivce has been set to NodePort tcp/30432 but at this stage I cannot access the port. The GCP vpc setup is new to me. I have been trying to set firewall rules
```sh
gcloud compute firewall-rules create gke-nodeport-postgresql --allow tcp:30080,tcp:30443,tcp:30432 --network gke-interview-test

gcloud compute firewall-rules list
NAME                                 NETWORK             DIRECTION  PRIORITY  ALLOW                          DENY  DISABLED
default-allow-icmp                   default             INGRESS    65534     icmp                                 False
default-allow-internal               default             INGRESS    65534     tcp:0-65535,udp:0-65535,icmp         False
default-allow-rdp                    default             INGRESS    65534     tcp:3389                             False
default-allow-ssh                    default             INGRESS    65534     tcp:22                               False
gke-gke-interview-test-53b71b6a-all  gke-interview-test  INGRESS    1000      esp,ah,sctp,tcp,udp,icmp             False
gke-gke-interview-test-53b71b6a-ssh  gke-interview-test  INGRESS    1000      tcp:22                               False
gke-gke-interview-test-53b71b6a-vms  gke-interview-test  INGRESS    1000      icmp,tcp:1-65535,udp:1-65535         False
myservicgke-nodeport-postgresql      default <-issue_was INGRESS    1000      tcp:30080,tcp:30443,tcp:30432        False
```

These are nodes with theirs public IPs
```sh
gcloud compute instances list
NAME                                                 ZONE            MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP     STATUS
gke-gke-interview-te-default-node-poo-007f720c-62nq  europe-west1-b  e2-medium                  10.10.0.5    35.240.110.238  RUNNING
gke-gke-interview-te-default-node-poo-f90fc58f-5ryn  europe-west1-c  e2-medium                  10.10.0.6    34.77.253.54    RUNNING
gke-gke-interview-te-default-node-poo-5946bb23-gx1r  europe-west1-d  e2-medium                  10.10.0.7    34.79.130.171   RUNNING
```

But unfortunately the port test timed out.
```sh
nc -zv 34.79.130.171 30432
Connection to 34.79.130.171 30432 port [tcp/*] succeeded!
```

# (fixed) Login to database fails
```sh
# Get the password for "postgres"
export POSTGRES_ADMIN_PASSWORD=$(kubectl get secret --namespace postgresql postgresql -o jsonpath="{.data.postgresql-postgres-password}" | base64 --decode)
# Get the password for "pi-user"
export POSTGRES_PASSWORD=$(kubectl get secret --namespace postgresql postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
export NODE_PORT=$(kubectl get --namespace postgresql -o jsonpath="{.spec.ports[0].nodePort}" services postgresql)
export NODE_IP=35.240.110.238

# Working connction
PGPASSWORD="$POSTGRES_ADMIN_PASSWORD" psql --host $NODE_IP --port $NODE_PORT -U postgres
psql (12.8 (Ubuntu 12.8-0ubuntu0.20.04.1), server 11.13)
Type "help" for help.

postgres=# \l
                                   List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |    Access privileges
-----------+----------+----------+-------------+-------------+-------------------------
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 predictx  | pi-user  | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/"pi-user"          +
           |          |          |             |             | "pi-user"=CTc/"pi-user"
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres            +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres            +
           |          |          |             |             | postgres=CTc/postgres
(4 rows)

# ! Not working
PGPASSWORD="$POSTGRES_PASSWORD" psql --host $NODE_IP --port $NODE_PORT -U pi-user -d predictx
psql: error: FATAL:  password authentication failed for user "pi-user"

# redeployed - fixed
PGPASSWORD="$POSTGRES_PASSWORD" psql --host $NODE_IP --port $NODE_PORT -U pi-user -d predictx
psql (12.8 (Ubuntu 12.8-0ubuntu0.20.04.1), server 11.13)
Type "help" for help.

predictx=> \l
                                   List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |    Access privileges
-----------+----------+----------+-------------+-------------+-------------------------
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 predictx  | pi-user  | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/"pi-user"          +
           |          |          |             |             | "pi-user"=CTc/"pi-user"
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres            +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres            +
           |          |          |             |             | postgres=CTc/postgres
(4 rows)

predictx=>
```
