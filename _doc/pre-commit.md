# Pre-commit
Advantage of using pre-commit ensure we do not commit something that can be verified allowing more consistency.


In example below end-of-lines have been fixed and trailing-whitespaces, the commit was prohibitted, files fixed and the subsequent commit suceeded.
```sh
git commit -am "adding sops (wip)"
[WARNING] Unexpected key(s) present on git://github.com/pre-commit/pre-commit-hooks: sha
[WARNING] Unexpected key(s) present on git://github.com/antonbabenko/pre-commit-terraform.git: sha
Check for byte-order marker..............................................Passed
Check for case conflicts.................................................Passed
Check for merge conflicts................................................Passed
Detect AWS Credentials...................................................Passed
Detect Private Key.......................................................Passed
Fix End of Files.........................................................Failed
- hook id: end-of-file-fixer
- exit code: 1
- files were modified by this hook

Fixing Makefile
Fixing apps/postgresql/values.yaml

Mixed line ending........................................................Passed
Trim Trailing Whitespace.................................................Failed
- hook id: trailing-whitespace
- exit code: 1
- files were modified by this hook

Fixing Makefile

Terraform fmt........................................(no files to check)Skipped
Terraform validate...................................(no files to check)Skipped
Terraform docs.......................................(no files to check)Skipped
Shellcheck Bash Linter...................................................Passed
gofmt................................................(no files to check)Skipped
piotr@Sylwia-zonka:~/src/myprojects/predictx.com/tf-gcp-gke$ git commit -am "addig sops (wip)"
[WARNING] Unexpected key(s) present on git://github.com/pre-commit/pre-commit-hooks: sha
[WARNING] Unexpected key(s) present on git://github.com/antonbabenko/pre-commit-terraform.git: sha
Check for byte-order marker..............................................Passed
Check for case conflicts.................................................Passed
Check for merge conflicts................................................Passed
Detect AWS Credentials...................................................Passed
Detect Private Key.......................................................Passed
Fix End of Files.........................................................Passed
Mixed line ending........................................................Passed
Trim Trailing Whitespace.................................................Passed
Terraform fmt........................................(no files to check)Skipped
Terraform validate...................................(no files to check)Skipped
Terraform docs.......................................(no files to check)Skipped
Shellcheck Bash Linter...................................................Passed
gofmt................................................(no files to check)Skipped
[initialize 71a0ae1] addig sops (wip)
 3 files changed, 33 insertions(+), 4 deletions(-)
 create mode 100644 apps/postgresql/values.yaml
```
