module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 3.4.0"

  project_id   = local.gcp_project_id
  network_name = "gke-interview-test"
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name      = "subnet-01"
      subnet_ip        = "10.10.0.0/21"
      subnet_region    = "europe-west1"
      subnet_flow_logs = "false"
      description      = "public"
    },
    {
      subnet_name           = "subnet-02"
      subnet_ip             = "10.10.24.0/21"
      subnet_region         = "europe-west1"
      subnet_private_access = "true"
      subnet_flow_logs      = "false"
      description           = "private"
    },
  ]

  secondary_ranges = {
    subnet-01 = [
      {
        range_name    = "subnet-01-secondary-01-pods"
        ip_cidr_range = "192.168.0.0/21" # 2048
      },
      {
        range_name    = "subnet-01-secondary-02-services"
        ip_cidr_range = "192.168.8.0/21" # 2048
      },
    ]

    subnet-02 = [
      {
        range_name    = "subnet-02-secondary-01"
        ip_cidr_range = "192.168.16.0/21" # 2048
      },
      {
        range_name    = "subnet-02-secondary-02"
        ip_cidr_range = "192.168.24.0/21" # 2048
      },
    ]
  }

  routes = [
    {
      name              = "egress-internet"
      description       = "route through IGW to access internet"
      destination_range = "0.0.0.0/0"
      tags              = "egress-inet"
      next_hop_internet = "true"
    },
  ]
}
