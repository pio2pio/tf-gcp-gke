terraform {
  backend "gcs" {
    bucket = "test1-devops-candiate1-bucket1"
    prefix = "terraform/state"
  }

  required_providers {
    external   = "2.1.0"
    google     = ">= 3.39.0, < 4.0.0" # = "3.83.0"
    kubernetes = "2.4.1"
    local      = "2.1.0"
    null       = "3.1.0"
    random     = "3.1.0"
    template   = "2.2.0"
  }
}


# google_client_config and kubernetes provider must be explicitly specified like the following.
data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

provider "google" {
  credentials = file("~/.config/gcloud/application_default_credentials.json")
  project     = local.gcp_project_id
  region      = "europe-west1"
  zone        = "europe-west1-b"
}
