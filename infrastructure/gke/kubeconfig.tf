module "gke_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"

  project_id           = local.gcp_project_id
  cluster_name         = local.gke_cluster_name
  location             = module.gke.location
  use_private_endpoint = false
}

# kubeconfig output
resource "local_file" "kubeconfig" {
  content         = module.gke_auth.kubeconfig_raw
  filename        = "${path.module}/kubeconfig"
  file_permission = "0600"
}
