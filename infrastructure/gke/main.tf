locals {
  gcp_project_id   = "test-devops-candidate1"
  gke_cluster_name = "gke-interview-test"
  gke_region       = "europe-west1"
}

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  version                    = "16.1.0"
  project_id                 = local.gcp_project_id
  name                       = local.gke_cluster_name
  region                     = local.gke_region
  zones                      = ["europe-west1-b", "europe-west1-c", "europe-west1-d"]
  network                    = module.vpc.network_name
  subnetwork                 = module.vpc.subnets_names[0]
  ip_range_pods              = "subnet-01-secondary-01-pods"
  ip_range_services          = "subnet-01-secondary-02-services"
  http_load_balancing        = false
  horizontal_pod_autoscaling = true
  network_policy             = false
  cluster_autoscaling        = local.cluster_autoscaling

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "e2-medium"
      node_locations     = "europe-west1-b,europe-west1-c,europe-west1-d"
      min_count          = 3
      max_count          = 10
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = false
      auto_upgrade       = false
      service_account    = "terraform@test-devops-candidate1.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    "all" : {},
    "default-node-pool" : {}
  }

  node_pools_taints = {
    all = []

    default-node-pool = [
      {
        key    = "default-node-pool"
        value  = true
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}

locals {
  cluster_autoscaling = {
    "enabled" : true,
    "gpu_resources" : [],
    "max_cpu_cores" : 1,
    "max_memory_gb" : 1,
    "min_cpu_cores" : 1,
    "min_memory_gb" : 1
  }
}
